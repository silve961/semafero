public class Semaforo{

    String colore = "rosso";
    int tempo = 50, tempoinizio = 0;

    public Semaforo(int bip){
        tempo = bip;
    }
    public int inizio(){
        while (true) {
            if (tempoinizio < 0) {
                intervallo();
                System.out.println(toString());
                tempoinizio = tempo;
            }
            tempoinizio--;
        }
    }
    public String toString(){
        return colore;
    }
    public void intervallo(){
        if (colore.equals("rosso")){
            colore="verde";
        }else if(colore.equals("arancione")){
            colore="rosso";
        }else colore="arancione";
    }

}
